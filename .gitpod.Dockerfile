FROM gitpod/workspace-base

USER gitpod

# Install Julia
RUN sudo apt-get update \
    && sudo apt-get install -y \
        julia \
    && sudo rm -rf /var/lib/apt/lists/*

# Give control back to Gitpod Layer
USER root
