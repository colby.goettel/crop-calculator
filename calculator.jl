#!/usr/bin/julia

# Let x = number of seed 1
# Let x' = price of seed 1
# Let y = number of seed 2
# Let y' = price of seed 2
# Let m = amount of money we have
# Let z = amount of seeds needed

# Find intersection of:
# m = (x*x')+(y*y')
# x + y = z

# Floor x and ciel y for results

# Initialize variables.
"The seed type we want to maximize."
primary_seed_type = 0
"The other, cheaper seed we have to buy to maximize purchasing power."
secondary_seed_type = 0
"Total amount of seeds needed."
seeds_needed = 0
"Each type of seed and its price."
seeds = Dict(
    "Amaranth" => 70,
    "Artichoke" => 30,
    "Beet" => 20,
    "Blue Jazz" => 30,
    "Blueberry" => 80,
    "Bok Choy" => 50,
    "Cauliflower" => 80,
    "Corn" => 150,
    "Cranberries" => 240,
    "Eggplant" => 20,
    "Fairy Rose" => 200,
    "Garlic" => 40,
    "Grape" => 200,
    "Green Bean" => 60,
    "Hops" => 60,
    "Hot Pepper" => 40,
    "Kale" => 70,
    "Melon" => 80,
    "Parsnip" => 20,
    "Poppy" => 100,
    "Potato" => 50,
    "Pumpkin" => 100,
    "Radish" => 40,
    "Red Cabbage" => 100,
    "Rhubarb" => 100,
    "Rice Shoot" => 40,
    "Starfruit" => 400,
    "Strawberry" => 100,
    "Summer Spangle" => 50,
    "Sunflower" => 200,
    "Tomato" => 50,
    "Tulip" => 20,
    "Wheat" => 10,
    "Yam" => 60,
)

# Functions
"""
    calculate_rows()

Calculate the total number of rows needed to print seeds in four columns.

This is done by taking the number of seeds, dividing by 4, and rounding down.
"""
function calculate_rows()
    return fld(length(seeds), 4)
end

"""
    get_positive_number_input(prompt::String)

Prints user prompt, reads user input, validates that it's a positive integer, and returns the user input.
"""
function get_positive_number_input(prompt::String)
    user_input = 0
    while typeof(user_input) == Int && !(user_input > 0)
        print(prompt, " (>0) ")
        user_input = parse(Int, readline())
    end
    return user_input
end

"""
    get_seed_type()

Reads user input for which type of seed they need and validates the number.
"""
function get_seed_type()
    user_input = 0
    while !(1 ≤ user_input ≤ length(seeds))
        print_seeds()
        print("Which type of seed do you need? (1-", length(seeds), ") ")
        user_input = parse(Int, readline())
    end
    return user_input
end

"""
    print_header()

Prints the program header.
"""
function print_header()
    header = raw"
Welcome to the unofficial Stardew Valley how-many-seeds-can-I-buy calculator.

You only have a certain amount of money and you want to maximize the amount of a certain
crop that you can grow, but you still want to plant every available space.

For example, it's spring and you have 1000g and want to maximize potatoes, but you only
have 25 open spots. 25 spots * 50 g = 1250 g. You can't afford to only plant potatoes.

This tool will let you pick a cheaper crop (say, parsnips) and it will calculate how many
of each to buy.
"
    println(header)
end

"""
    print_seeds()

Prints an enumerated list of each seed in 4 columns.
"""
function print_seeds()
    count = 1
    rows = calculate_rows()
    # Run through each key in seed Dict, sorted alphabetically by key.
    for (key, value) in sort(collect(seeds), by=x -> x[1])
        print(count, " ", key, "\t\t")
        count += 1
        # Print four columns.
        if (count - 1) % 4 == 0
            print("\n")
        end
    end
    println("\n")
end

# Main
print_header()

# Gather user input.
total_funds = get_positive_number_input("How much money do you have?")
seeds_needed = get_positive_number_input("How many spaces are you trying to fill?")
primary_seed_type = get_seed_type()
secondary_seed_type = get_seed_type()

println(
    "total_funds: ", total_funds,
    "; seeds_needed: ", seeds_needed,
    "; primary_seed_type:", primary_seed_type
)

# secondary_seed_quantity# total_funds = (seed1_price*primary_seed_type)+(seed2_price*seed2_amount)
